﻿# MindSpore SciAI 0.1.0 Release Notes

Initial release of MindSpore SciAI.

[查看中文](./RELEASE_CN.md)

### Major Feature

- [STABLE] [`High-frequency models library`](./sciai/model): Built-in 60+ high-frequency models which ranks No.1 in the world in terms of coverage. High-frequency models cover from physics-informed (such as PINNs, DeepRitz and PFNN, etc.) and neural operators (such as FNO, DeepONet).
- [STABLE] [`High-level API`](./README.md#Quick-Start): Developers and users with high-level APIs (for example AutoModel) allow an immediate deployment.
- [STABLE] [`Basic API`](./sciai): Provides basic APIs (such as Fourier neural operators, adaptive activation functions and high-order differentiation, etc.), which are convenient for users to build, train and inference neural network models of AI4SCI.

### Contributors

Thanks goes to these wonderful people:

yufan, wangzidong, yangkang, lujiale, lizhihao, huyintong, zhangrenyuan, mazhiming.