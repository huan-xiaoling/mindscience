sponge.function.get_length_unit
===================================

.. py:function:: sponge.function.get_length_unit(unit)

    获得长度单位。

    参数：
        - **unit** (Union[str, Units, Length, float, int]) - 长度单位。

    返回：
        str。长度单位。