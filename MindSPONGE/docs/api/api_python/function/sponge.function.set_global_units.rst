sponge.function.set_global_units
====================================

.. py:function:: sponge.function.set_global_units(length_unit: Union[str, Units, Length, float, int] = None, energy_unit: Union[str, Units, Length, float, int] = None, units: Units = None)

    给全局单位设置单位。

    参数：
        - **length_unit** (Union[str, Units, Length, float, int]) - 长度单位。
        - **energy_unit** (Union[str, Units, Length, float, int]) - 能量单位。
        - **units** (Units) - 单位。