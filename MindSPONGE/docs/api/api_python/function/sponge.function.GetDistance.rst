sponge.function.GetDistance
===============================

.. py:class:: sponge.function.GetDistance(use_pbc: bool = None, keepdims: bool = False, axis: int = -1)

    获取有或者没有PBC box的距离。

    参数：
        - **use_pbc** (bool) - 计算距离时是否使用周期性边界条件。默认值： ``None`` 。
        - **keepdims** (bool) - 是否保留最后一个维度。默认值： ``False`` 。
        - **axis** (int) - 计算距离时是否使用周期性边界条件。默认值： ``-1`` 。
